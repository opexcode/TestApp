//
//  AppViewModel.swift
//  TestApp
//
//  Created by   imac on 13.09.2022.
//

import SwiftUI

class AppViewModel: ObservableObject {
    
    @Published var content: Sections = Bundle.main.decode("jsonviewer.json")
    
}
