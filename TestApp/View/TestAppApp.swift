//
//  TestAppApp.swift
//  TestApp
//
//  Created by   imac on 13.09.2022.
//

import SwiftUI

@main
struct TestAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
