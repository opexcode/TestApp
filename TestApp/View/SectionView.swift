//
//  SectionView.swift
//  TestApp
//
//  Created by   imac on 13.09.2022.
//

import SwiftUI

struct SectionView: View {
    
    let section: Section
    
    var body: some View {
        VStack(alignment: .leading, spacing: 15) {
            Text(section.header)
                .font(.title2)
                .fontWeight(.bold)
                .padding(.horizontal)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 15) {
                    ForEach(section.items) { content in
                        SectionViewCell(item: content)
                    }
                }
                .padding(.horizontal)
            }
        }
    }
    
}
