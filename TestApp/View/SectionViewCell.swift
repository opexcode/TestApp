//
//  SectionViewCell.swift
//  TestApp
//
//  Created by   imac on 13.09.2022.
//

import SwiftUI


struct SectionViewCell: View {
    
    let item: Item
    
    @State private var isSelected = false
    
    var body: some View {
        let imageUrl = "https://api.lorem.space/image/movie?w=150&h=220"
        
        ZStack(alignment: .bottom) {
            AsyncImage(url: URL(string: imageUrl)) { image in
                image.resizable()
            } placeholder: {
                placeholder
            }
            
            Text(item.title)
                .font(.callout)
                .fontWeight(.bold)
                .frame(maxWidth: .infinity)
                .padding(10)
                .background(.white)
                .opacity(0.8)
        }
        .frame(width: 170)
        .overlay(selectedCell)
        .cornerRadius(20)
        .onTapGesture {
            isSelected.toggle()
            // TODO show article
        }
    }
    
    @ViewBuilder
    var selectedCell: some View {
        if isSelected {
            RoundedRectangle(cornerRadius: 20)
                .strokeBorder(lineWidth: 3)
                .foregroundColor(.red)
                .opacity(0.6)
        }
    }
    
    var placeholder: some View {
        ProgressView()
            .frame(width: 170, height: 220)
            .aspectRatio(3/4, contentMode: .fill)
    }
    
}
