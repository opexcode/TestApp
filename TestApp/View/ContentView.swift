//
//  ContentView.swift
//  TestApp
//
//  Created by   imac on 13.09.2022.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject private var viewModel = AppViewModel()
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 40) {
                ForEach(viewModel.content.sections) { content in
                    SectionView(section: content)
                }
            }
            .padding(.vertical)
        }
        .background(Color(.systemGray5))
    }
    
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
